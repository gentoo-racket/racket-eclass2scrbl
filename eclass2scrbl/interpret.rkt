;; This file is part of racket-eclass2scrbl - Eclass to Scribble converter.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-eclass2scrbl is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-eclass2scrbl is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-eclass2scrbl.  If not, see <https://www.gnu.org/licenses/>.


#lang racket/base

(require syntax/parse
         (only-in racket/file file->string)
         (only-in racket/match match)
         (only-in racket/string string-join)
         (only-in threading ~>> lambda~>>)
         "content.rkt"
         "parse.rkt"
         "tokenize.rkt")

(provide (all-defined-out))


(define (source->docs stx)
  (syntax-parse stx
    [({~literal source} element-stxs ...)
     (for/list ([element-stx (syntax->list #'(element-stxs ...))]
                #:when (equal? (car (syntax->datum element-stx)) 'doc))
       (syntax->datum element-stx))]))


(define (interpret-element element)
  (match element
    [`(header (,name ,content))
     (case name
       [("ECLASS")
        (~>> content
             (regexp-replace #rx"\\.eclass$" _ "")
             string-titlecase
             (format "@title{~a}\n@section{Eclass}\n"))]
       [("ECLASS_VARIABLE" "FUNCTION" "VARIABLE")
        (string-append (format "@section{~a}" (clean-content content))
                       "\n"
                       (format "@itemlist[@item{@exec{~a}}]\n"
                               (clean-content name))
                       "\n")])]
    [`(tag (tag-inline (,name ,content)))
     (format "@subsection[#:tag \"~a ~a\"]{~a}\n~a\n"
             (clean-content name)
             (clean-tag content)
             (string-titlecase name)
             (~>> content
                  format-content))]
    [`(tag (tag-multi ,name))
     (~>> name
          clean-content
          string-titlecase
          (format "@subsection{~a}\n"))]
    [`(tag (tag-multi ,name) ,content ...)
     (format "@subsection[#:tag \"~a ~a\"]{~a}\n~a\n"
             (clean-content name)
             (~>> content
                  car
                  clean-tag)
             (string-titlecase name)
             (~>> content
                  (string-join _ "\n")
                  format-content))]
    [`(property ,name)
     (~>> name
          clean-content
          (format "@itemlist[@item{@exec{~a}}]\n"))]
    [`(sub ,name)
     (~>> name
          clean-content
          (format "@subsubsection{~a}\n"))]
    [`(example #f ,content ... #f)
     (format "@nested[#:style 'code-inset]{@verbatim{\n~a}}\n"
             (~>> content
                  (string-join _ "\n")
                  clean-content))]
    [`(comment ,content ...)
     (~>> content
          (string-join _ "\n")
          format-content)]))


(define (interpret-source stx)
  (~>> stx
       source->docs
       (filter (lambda (l) (equal? (car l) 'doc)))
       (map (lambda~>>
             cdr
             (map interpret-element)
             (string-join _ "\n")))
       (string-join _ "\n")))

(define (parse-eclass-file file-path)
  (~>> file-path
       file->string
       tokenize
       parse))

(define (eclass->documentation file-path)
  (~>> file-path
       parse-eclass-file
       interpret-source))
