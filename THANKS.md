# Thank You to


+ Jens Axel Søgaard (soegaard)
  for:
  - help with parser,
    commit: `a600659e9997321ea1f111147a0ef8345f26846f`

+ William G Hatch (willghatch)
  for:
  - Scribble block formattign from Linea,
    code: `@nested[#:style 'code-inset]{@verbatim{}}`,
    commit: `c7f19bc6b1f5462f8884017ca35b8d509bad10ce`
